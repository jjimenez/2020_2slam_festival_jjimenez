<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>RepresentationDAO : test</title>
    </head>

    <body>

        <?php

        /*
        use modele\metier\Representation;
        use modele\metier\Offre;
        use modele\metier\Lieu;
        use modele\metier\Groupe;
        use PDO;
        */
        use modele\dao\RepresentationDAO;
        use modele\dao\Bdd;
        use controleur\Session;

require_once __DIR__ . '/../../includes/autoload.inc.php';

        $id = "r001";
        Session::demarrer();
        Bdd::connecter();

        echo "<h2>Test RepresentationDAO</h2>";

        // Test n°1
        echo "<h3>1- Test getOneById</h3>";
        try {
            $objet = RepresentationDAO::getOneById($id);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
          // Test n°2
        echo "<h3>2- Test getAll</h3>";
        try {
            $lesObjets = RepresentationDAO::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        //Test n°3
         echo "<h3>2- Test getAllDates</h3>";
        try {
            $lesObjets = RepresentationDAO::getAllDates();
            var_dump($lesObjets);
            print_r($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }
        
        Bdd::deconnecter();
        Session::arreter();
        ?>


    </body>
</html>


