<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Representation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Groupe;
        use modele\metier\Lieu;
        
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Representation</h2>";
        
        $id =('1');
        $unGroupe = new Groupe("g999","les Joyeux Turlurons","général Alcazar","Tapiocapolis" ,25,"San Theodoros","O");
        $unLieu = new Lieu("l1","Nantes","30 rue du test",15);
        $date = ("18/05/2017");
        $heureDebut = ("20:30");
        $heureFin = ("22:30");
        
        $objet = new Representation($id ,$unGroupe, $unLieu, $date, $heureDebut, $heureFin);
        var_dump($objet);
        ?>
    </body>
</html>