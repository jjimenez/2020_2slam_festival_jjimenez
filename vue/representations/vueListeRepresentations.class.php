<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\metier\Lieu;
use modele\metier\Groupe;
use modele\dao\GroupeDAO;
use modele\dao\LieuDAO;

ini_set('display_errors', 'on');

class VueListeRepresentations  extends VueGenerique {
    private $lesRepresentations;
    
    
        public function __construct() {
        parent::__construct();
    }
    public function afficher() {
     include $this->getEntete();
     
?>

<h2>Les représentations</h2>
<br>

<?php
            $DateTest="0000-00-00";
            foreach ($this->lesRepresentations as $uneRepresentation) {         
                     $uneDate= $uneRepresentation->getDate();
                     
                     if ($DateTest!=$uneDate){
                        $DateTest=$uneDate;
                ?>
</table>      
<h3><?= $uneDate?></h3>
            

<table width="50%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                    
                     <tr class="enTeteTabQuad">
                        <td width="30%">Lieu</td>
                        <td width="30%">Groupe</td> 
                        <td width="10%">Heure Début</td> 
                        <td width="10%">Heure Fin</td> 
                        <td width="10%"></td> 
                        <td width="10%"></td> 
                    </tr>
                        <tr class="ligneTabQuad">
                        <td ><?= LieuDAO::getOneById($uneRepresentation->getLieu())->getNom()?></td>
                        <td><?= GroupeDAO::getOneById($uneRepresentation->getGroupe())->getNom() ?></td> 
                        <td><?=$uneRepresentation->getHeureDebut()?></td> 
                        <td><?=$uneRepresentation->getHeureFin()?></td> 
                        <td><a href="index.php?controleur=representations&action=modifier&id=<?= $uneRepresentation->getId() ?>" >Modifier</a></td>
                        <td><a href="index.php?controleur=representations&action=supprimer&id=<?= $uneRepresentation->getId() ?>" >Supprimer</a></td>
                    </tr>

                    
        <br>
 <?php 
            }else{
                
            ?>
                
                        <tr class="ligneTabQuad">
                        <td ><?= LieuDAO::getOneById($uneRepresentation->getLieu())->getNom()?></td>
                        <td><?= GroupeDAO::getOneById($uneRepresentation->getGroupe())->getNom()?></td> 
                        <td><?=$uneRepresentation->getHeureDebut()?></td> 
                        <td><?=$uneRepresentation->getHeureFin()?></td> 
                       <td><a href="index.php?controleur=representations&action=modifier&id=<?= $uneRepresentation->getId() ?>">Modifier</a></td>
                       <td><a href="index.php?controleur=representations&action=supprimer&id=<?= $uneRepresentation->getId() ?>" >Supprimer</a></td>
                    </tr>
<?php
            }
            }
            ?>
      
 <?php
        include $this->getPied();
    }

    function setLesRepresentations($lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }

}

    
            